# Installation

```bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm upgrade -i kube-prometheus-stack prometheus-community/kube-prometheus-stack --version 39.6.0 --values custom-values.yaml -n metrics
```

# Prometheus

# AlertManager

# Grafana